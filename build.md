# Build Instructions

1. Install Ubuntu 16.04
    - Only tested on x86 CPU's
    - This can be a VM or baremetal, server or desktop or laptop, etc
2. Install updates
    - sudo apt update && sudo apt upgrade
3. Reboot
    - sudo reboot
4. Install dependencies
    - sudo apt install build-essential git cmake libboost-all-dev screen
5. Clone this repository 
    - git clone https://gitlab.com/gntlcoin/gntlcoin-core.git
6. Change your directory and then kick off the build
    - cd gntlcoin-core
    - make
7. Once the build is complete (will take around 18 minutes on a single processer thread), copy out binaries
    - mkdir ~/gntlcoin-bin
    - cp ./build/release/src/connectivity_tool ~/gntlcoin_bin/
    - cp ./build/release/src/gntlcoind ~/gntlcoin_bin/
    - cp ./build/release/src/miner ~/gntlcoin_bin/
    - cp ./build/release/src/simplewallet ~/gntlcoin_bin/
    - cp ./build/release/src/walletd ~/gntlcoin_bin/
8. Open screen, start the daemon to sync the blockchain, and then run in the background
    - cd ~/gntlcoin-bin
    - screen (you may need to press enter to get past a splash screen and back to a prompt)
    - ./gntlcoind
    - Type "help" and press enter to see some additional commands you can run
    - press the control key and the A key at the same time, then release then press the D key
    - This will detach the screen session and have that terminal window run in the background
    - This lets the daemon run even after you have disconnected from the SSH session or closed the terminal window
9. Generate a wallet
    - ./simplewallet --generate-new-wallet ~/gntlcoin-wallet
    - The last argument is the path whewre you want the wallet file to be created
    - You can specifiy that wherever you want, but remember where it's stored!
    - Type in a strong, secure password to setup your wallet
    - Type "help" and press enter to see some additional commands you can run
    - If you just want to run a node or have a wallet, you can stop here
    - if you want to mine as well, copy your address to your clipboard or a scratch text file or so on
10. Start mining!
    - screen -r
    - This will resume that terminal session you had hidden in the background
    - start_mining PASTE_YOUR_ADDRESS_HERE 1
    - Replace the "1" above with the number of threads you want to mine with. 
    - You can leave it running and close your SSH session/terminal window by pressing control + A then releasing and pressing D
11. Checking on your wallet balance / If you want to see the coins roll in:
    - cd ~/gntlcoin_bin
    - ./simplewaller --wallet-file ~/gntlcoin-wallet
    - The path there is the same path you typed when generating the wallet
    - Type the password you used to create the wallet to unlock it
    - Type "balance" and press enter to view your available / locked (pending) balances.
